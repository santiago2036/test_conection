from fastapi.testclient import TestClient
from app import app
import os

client = TestClient(app)

def test_Generate_docs():
    response = client.get(f"/test/")
    assert response.status_code == 200