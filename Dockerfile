FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8
COPY requirements.txt /usersapi/requirements.txt
WORKDIR /usersapi
RUN python -m pip install -r requirements.txt
COPY . /usersapi
CMD ["uvicorn", "app:app", "--host", "0.0.0.0", "--port", "8010", "--reload"]
