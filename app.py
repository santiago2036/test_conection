from fastapi import FastAPI
from routes.test import test

app = FastAPI(
    title="Test Conection",
    description="API to get started with FastAPI",
    version="1.0.0",
)

app.include_router(test)
